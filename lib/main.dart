import 'dart:io';
import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';

import 'package:geot/pages/set_dir.dart';
import 'package:geot/pages/notes.dart';
import 'package:geot/models/app_data.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    setWindowTitle(appData.title);
    setWindowMinSize(const Size(600, 400));
    setWindowMaxSize(Size.infinite);
  }

  runApp(const GeotApp());
}

class GeotApp extends StatelessWidget {
  const GeotApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.green),
        initialRoute: appData.ndir.exists() ? NotesPage.routeName : WarningPage.routeName,
        routes: {
          WarningPage.routeName: (context) => const WarningPage(),
          NotesPage.routeName: (context) => const NotesPage(),
        });
  }
}
