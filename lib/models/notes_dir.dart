import 'dart:io';

import 'package:geot/components/notes_menu.dart';

class NotesDir {
  String get path {
    const homeDir = String.fromEnvironment('HOME', defaultValue: '/home/user');
    const geotDir =
        String.fromEnvironment('GEOT_DIR', defaultValue: '$homeDir/.geot');
    return geotDir;
  }

  Directory get dir {
    return Directory(path);
  }

  bool exists() {
    return dir.existsSync();
  }

  List<NotesMenuItem> notesTreeItems([List<FileSystemEntity>? dirListing]) {
    dirListing ??= _list();
    List<NotesMenuItem> items = [];
    for (var entity in dirListing) {
      if (entity is File) {
        var widget = NoteItem(entity: entity);
        items.add(widget);
      } else if (entity is Directory) {
        List<NotesMenuItem> children = notesTreeItems(_list(entity));
        var widget = FolderItem(entity: entity, children: children);
        items.add(widget);
      }
    }
    return items;
  }

  List<FileSystemEntity> _list([Directory? dirToList]) {
    dirToList ??= dir;
    return dirToList.listSync(recursive: false, followLinks: false);
  }
}
