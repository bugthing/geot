import 'dart:io';
import 'package:flutter/material.dart';
import 'package:geot/models/notes_dir.dart';

class AppData {
  static final AppData _appData = new AppData._internal();
  
  String title = "Geot it down..";
  NotesDir ndir = NotesDir();
  File? note;

  factory AppData() {
    return _appData;
  }

  AppData._internal();
}

final appData = AppData();
