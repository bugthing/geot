import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geot/components/notes_menu.dart';
import 'package:geot/components/no_note.dart';
import 'package:geot/components/note_info.dart';
import 'package:geot/components/note_title.dart';
import 'package:geot/models/app_data.dart';

class NotesPage extends StatefulWidget {
  const NotesPage({Key? key}) : super(key: key);

  static const routeName = '/notes';

  @override
  State<NotesPage> createState() => _NotesPageState();
}

class _NotesPageState extends State<NotesPage> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: appData.title,
      home: Scaffold(
        appBar: AppBar(title: TitleBar()),
        body: Row(
          children: [
            Expanded(flex: 1, child: NotesMenu()),
            Expanded(flex: 2, child: NoteInfo()),
          ]
        )
      ),
    );
  }
}
