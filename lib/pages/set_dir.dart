import 'package:flutter/material.dart';

import 'package:geot/models/app_data.dart';

class WarningPage extends StatelessWidget {
  const WarningPage({Key? key}) : super(key: key);

  static const routeName = '/warning';

  @override
  Widget build(BuildContext context) {
    var pathSet = appData.ndir.path;

    return Scaffold(
      appBar: AppBar(title: const Text('Please configure!')),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            'Please set GEOT_DIR environment variable to point to the directory containing markdown files: $pathSet',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
      ),
    );
  }
}




