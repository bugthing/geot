import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:geot/models/app_data.dart';

class NoteInfo extends StatefulWidget {
  NoteInfo({Key? key}) : super(key: key);

  File? file = appData.note;

  @override
  State<NoteInfo> createState() => _NoteInfoState();
}

class _NoteInfoState extends State<NoteInfo> {
  @override
  Widget build(BuildContext context) {
    var file = widget.file;
    if (file == null) {
      return Icon(
        Icons.favorite,
        color: Colors.pink,
        size: 24.0,
        semanticLabel: 'No note selected',
      );
    } else {
      var markdown = file.readAsStringSync();
      return Markdown(data: markdown);
    }
  }
}

