import 'dart:io';
import 'package:flutter/material.dart';
import 'package:geot/models/app_data.dart';
import 'package:geot/pages/notes.dart';

class TitleBar extends StatelessWidget {
  const TitleBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var file = appData.note;
    if ( file == null ) {
      return Text(appData.title);
    } else {
      return NoteTitle(file);
    }
  }
}

class NoteTitle extends StatefulWidget {
  File file;

  NoteTitle(this.file, {Key? key}) : super(key: key);

  @override
  _NoteTitleState createState() => _NoteTitleState();
}

class _NoteTitleState extends State<NoteTitle> {
  bool isEditable=false;

  void _changeFileName(String newFileName) {
    var path = widget.file.path;
    var lastSeparator = path.lastIndexOf(Platform.pathSeparator);
    var newPath = path.substring(0, lastSeparator + 1) + newFileName;
    widget.file.renameSync(newPath);
    var newFile = File(newPath);
    appData.note = newFile;
    Navigator.of(context, rootNavigator: true).pushNamed(NotesPage.routeName);
  }

  String get title { 
    return widget.file.path.split(Platform.pathSeparator).last;
  }

  set title(String title) {  
    _changeFileName(title);
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: !isEditable
              ? Text(title)
              : TextFormField(
                  initialValue: title,
                  textInputAction: TextInputAction.done,
                  onFieldSubmitted: (value) {
                    setState(() => {isEditable = false, title = value});
                  })),
      IconButton(
        icon: Icon(Icons.edit),
        onPressed: () {
          setState(() => {
                isEditable = true,
              });
        },
      )
    ]);
  }
}
