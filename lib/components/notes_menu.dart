import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geot/pages/notes.dart';
import 'package:geot/models/app_data.dart';

class NotesMenu extends StatefulWidget {
  const NotesMenu({Key? key}) : super(key: key);

  @override
  State<NotesMenu> createState() => _NotesMenuState();
}

class _NotesMenuState extends State<NotesMenu> {
  @override
  Widget build(BuildContext context) {
    var items = appData.ndir.notesTreeItems();
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (context, index) {
        final item = items[index];

        stderr.writeln('item:' + item.entity.path);
        if (item is FolderItem) {
          stderr.writeln('kids:' + item.children.length.toString());
        }

        return ListTile(
          title: item,
        );
      },
    );
  }
}


class NotesMenuItem extends StatefulWidget {
  const NotesMenuItem({Key? key, required this.entity}) : super(key: key);

  final FileSystemEntity entity;

  @override
  State<NotesMenuItem> createState() => _NotesMenuItemState();
}

class _NotesMenuItemState extends State<NotesMenuItem> {
  @override
  Widget build(BuildContext context) {
    return Text('This is a NoteTreeItem');
  }
}

class NoteItem extends NotesMenuItem {
  const NoteItem({Key? key, required FileSystemEntity entity})
      : super(key: key, entity: entity);

  @override
  State<NoteItem> createState() => _NoteItemState();
}

class _NoteItemState extends State<NoteItem> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {

        appData.note = widget.entity as File;

        Navigator.of(context, rootNavigator: true).pushNamed(NotesPage.routeName);
      },
      child: Column(children: <Widget>[
        Text(
          widget.entity.path.split('/').last,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ]),
    );
  }
}

class FolderItem extends NotesMenuItem {
  final List<NotesMenuItem> children;

  const FolderItem({Key? key, entity, required this.children})
      : super(key: key, entity: entity);

  @override
  State<FolderItem> createState() => _FolderItemState();
}

class _FolderItemState extends State<FolderItem> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      const SizedBox(height: 20.0),
      ExpansionTile(
        key: PageStorageKey<String>(widget.entity.path),
        title: Text(
          widget.entity.path.split(Platform.pathSeparator).last,
          style: Theme.of(context).textTheme.subtitle2
        ),
        children: widget.children,
      ),
    ]);
  }
}
